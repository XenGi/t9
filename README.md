<img src="t9_back.png" width="900px" />
<img src="t9_front.png" width="900px" />

T9 PCB
======

A QMK based 6x4 numpad keyboard intended to act as a switch and keycap tester.

---

Made with ❤️ and [kicad](https://www.kicad.org/).
